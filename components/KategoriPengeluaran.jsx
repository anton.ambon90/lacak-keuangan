import React from 'react'
import { currancyFormatter } from '../lib/utils'

const KategoriPengeluaran = (props) => {
    const { total, color, title } = props
    return (
        <button>
            <div className='bg-gray-200 flex px-2 gap-2 items-center py-2 rounded-full  '>
                <div className=' h-5 w-5 rounded-full' style={{ backgroundColor: color }}></div>
                <div className=' flex justify-between w-full'>
                    <p>{title}</p>
                    <p>{currancyFormatter(total)}</p>
                </div>
            </div>
        </button>
    )
}

export default KategoriPengeluaran