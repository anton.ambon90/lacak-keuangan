const Navigation = () => {
    return (
        <header className='container max-w-2xl py-2  w-full justify-end px-4 items-center text-center flex mx-auto'>
            <button className='btn btn-danger'>Log Out</button>
        </header>
    )
}

export default Navigation