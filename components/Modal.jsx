import React from 'react'

const Modal = (props) => {
    const { onClick, title, bgColor, style, bgColorButton, styleOpen } = props
    return (

        < div style={{
            transform: style ? 'translateX(0%)' : styleOpen
        }}
            className='absolute top-0 right-0 w-full h-full z-10 transition-all duration-500' >
            <div className={`container mx-auto w-[90%]  rounded-3xl p-5 h-[80vh] ${bgColor}`}>
                <button onClick={() => onClick(false)} className={`h-10 w-10 ${bgColorButton} mb-5 rounded-full`}>X</button>
                <h3>{title}</h3>
                <input className='w-full rounded-xl p-2' type="search" placeholder='Search' />
            </div>
        </div >

    )
}

export default Modal