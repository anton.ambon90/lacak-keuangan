"use client"
import { Button, Input } from '@material-tailwind/react'
import React from 'react'

const Login = () => {
  return (
    <div className='flex mx-auto h-screen justify-center items-center text-center'>
        <form action="" className='w-72   '>
            <div className='flex flex-col gap-2 text-left '>
            <h1 className="text-lg mb-5 text-center">Login Email and Password</h1>
            <label>Email</label>
            <Input label="Email" />
            <label>Password</label>
            <Input label="Password" />
            <Button className='w-full'>Login</Button>
            <Button className='w-full'>Login With Google</Button>

            </div>
        </form>
    </div>
  )
}

export default Login