
export const currancyFormatter = (amount) => {
    const formatting = Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
        minimumFractionDigits: 0

    })
    return formatting.format(amount)
}

