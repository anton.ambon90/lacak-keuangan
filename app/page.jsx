"use client"
import KategoriPengeluaran from '@/components/KategoriPengeluaran';
import Login from '@/components/Login';
import Modal from '@/components/Modal';
import Nav from '@/components/Navigation'
import { currancyFormatter } from '@/lib/utils'

import { Chart as ChartJS, ArcElement, Tooltip, Legend } from "chart.js";
import { useState } from 'react';
import { Doughnut } from "react-chartjs-2";

ChartJS.register(ArcElement, Tooltip, Legend);


export default function Home() {
    const [openModal, setOpenModal] = useState(false)
    const [openModalPemasukan, setOpenModalPemasukan] = useState(false)



    const DUMMY_DATA = [
        {
            id: 1,
            title: 'Belanja',
            color: '#675657',
            total: 1000000
        },
        {
            id: 2,
            title: 'Makanan',
            color: '#544533',
            total: 1500000
        },
        {
            id: 3,
            title: 'Sedekah',
            color: '#543333',
            total: 500000
        },
        {
            id: 4,
            title: 'Medical',
            color: '#899989',
            total: 100000
        },
        {
            id: 5,
            title: 'Liburan',
            color: '#ffffff',
            total: 500000
        },
    ]
    return (

        <>
            <Modal style={openModal} styleOpen={'translateX(-100%)'} onClick={setOpenModal} title="Pengeluaran" bgColor="bg-gray-200" bgColorButton="bg-gray-300" />
            <Modal style={openModalPemasukan} styleOpen={'translateX(100%)'} onClick={setOpenModalPemasukan} title="Pemasukkan" bgColor="bg-green-200" bgColorButton="bg-green-300" />
            <main className=' container text-gray-800 font-bold  max-w-2xl mx-auto px-2'>
                <section className='py-3 text-center' >
                    <small className='text-md '>SALDO</small>
                    <h2 className='text-xl'>{currancyFormatter(5000000)}</h2>
                </section>
                <section className='flex gap-2 justify-center items-center px-6 jusstify-center'>
                    <button onClick={() => setOpenModal(true)} className='btn btn-primary'>- Pengeluaran</button>
                    <button onClick={() => setOpenModalPemasukan(true)} className='btn btn-primary-section'>+ Pemasukan</button>

                </section>
                {/* Pengeluaran */}
                <section className='py-6 px-2'>
                    <h3 className='px-2 text-1xl font-bold text-bg-blue-gray-800 mb-2'>Pengeluaran</h3>
                    <div className='flex gap-2 flex-col'>
                        {DUMMY_DATA.map((item) => (
                            <KategoriPengeluaran key={item.id} title={item.title} color={item.color} total={item.total} />
                        ))}
                    </div>
                </section>
                <section className='py-6 px-2 '>
                    <h3 className='px-2 text-1xl font-bold text-bg-blue-gray-800 mb-2'>Statistik</h3>
                    <div className='w-1/2 mx-auto'>
                        <Doughnut data={{
                            labels: DUMMY_DATA.map((item) => item.title),
                            datasets: [{
                                label: "Pengeluaran",
                                data: DUMMY_DATA.map((item) => item.total),
                                backgroundColor: DUMMY_DATA.map((item) => item.color),
                                borderColor: 'transparent',
                                borderWidth: 5,
                                borderAlign: 'center',
                            }]
                        }} /></div>
                </section>
            </main>
        </>
    )
}